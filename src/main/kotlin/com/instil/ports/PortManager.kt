package com.instil.ports

class PortManager(val min: Int, val max: Int) {

    var ports : Array<Boolean>

    init {
        if (min > max) throw IllegalArgumentException("Min should be less than max")
        ports = Array(max - min + 1){ false }
    }

    fun acquirePort(): Int {
        for (port in min..max) {
            val index = indexFor(port)
            if (!ports[index]) {
                ports[index] = true
                return port
            }
        }
        return -1
    }

    fun releasePort(port: Int) {
        val index = indexFor(port)
        ports[index] = false
    }

    private fun indexFor(port: Int): Int = port - min
}