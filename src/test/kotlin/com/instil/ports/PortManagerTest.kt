package com.instil.ports

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test

class PortManagerTest {

    @Test
    fun shouldCreatePortManagerWithRange() {
        val min = 1
        val max = 10
        val portManager = PortManager(min, max)

        assertThat(min).isEqualTo(portManager.min)
    }

    @Test
    fun shouldThrowExceptionIfMinIsGreaterThanMax() {
        assertThatThrownBy { PortManager(100, 1) }.isInstanceOf(IllegalArgumentException::class.java)
    }

    @Test
    fun shouldReturnNextAvailablePort() {
        val portManager = PortManager(1, 10)

        assertThat(portManager.acquirePort()).isEqualTo(1)
        assertThat(portManager.acquirePort()).isEqualTo(2)
        assertThat(portManager.acquirePort()).isEqualTo(3)
    }

    @Test
    fun shouldStartReturningFromMin() {
        val portManager = PortManager(10, 12)

        assertThat(portManager.acquirePort()).isEqualTo(10)
    }

    @Test
    fun shouldReturnMinusOneIfNoAvailablePorts() {
        val portManager = PortManager(10, 11)
        repeat(2) { portManager.acquirePort() }

        assertThat(portManager.acquirePort()).isEqualTo(-1)
    }

    @Test
    fun shouldMakeReleasedPortAvailable() {
        val portManager = PortManager(1, 5)
        val port = portManager.acquirePort()
        portManager.releasePort(port)

        assertThat(portManager.acquirePort()).isEqualTo(port)
    }

    @Test
    fun shouldReturnNextFreePortAfterReleaseAndAcquire() {
        val portManager = PortManager(1, 5)
        val firstPort = portManager.acquirePort()
        val secondPort = portManager.acquirePort()
        portManager.releasePort(firstPort)
        portManager.acquirePort()

        assertThat(portManager.acquirePort()).isEqualTo(secondPort + 1)
    }
}